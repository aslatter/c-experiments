#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <sys/types.h>
#include <unistd.h>

// See: ioctl_ns(7)

#ifndef NS_GET_USERNS
#define NSIO 0xb7
#define NS_GET_USERNS _IO(NSIO, 0x1)
#define NS_GET_PARENT _IO(NSIO, 0x2)
#endif

void print_file(const char *pathname);
void print_file_fd(int fd);
int open_or_die(const char *pathname, int flags);

int main() {
  printf("User id: %ju\n\n", (uintmax_t)getuid());

  puts("Original user ns:");
  print_file("/proc/self/ns/user");

  puts("Original uts ns:");
  print_file("/proc/self/ns/uts");

  if (unshare(CLONE_NEWUSER | CLONE_NEWUTS) == -1) {
    perror("Error in unshare");
    exit(EXIT_FAILURE);
  }

  printf("User id (after unshare): %ju\n\n", (uintmax_t)getuid());

  puts("New user ns:");
  print_file("/proc/self/ns/user");

  int uts_ns_fd = open_or_die("/proc/self/ns/uts", O_RDONLY);

  puts("New uts namespace: ");
  print_file_fd(uts_ns_fd);

  int owning_uns = ioctl(uts_ns_fd, NS_GET_USERNS);
  if (owning_uns == -1) {
    perror("Error getting owning user namespace");
    exit(EXIT_FAILURE);
  }

  puts("Owning user ns of new uts ns:");
  print_file_fd(owning_uns);

  close(owning_uns);
  close(uts_ns_fd);
}

int open_or_die(const char *pathname, int flags) {
  int fd = open(pathname, flags);
  if (fd == -1) {
    fprintf(stderr, "Error opening %s: %s", pathname, strerror(errno));
    exit(EXIT_FAILURE);
  }
  return fd;
}

void print_sb(const struct stat *sb) {
  printf("Device: %ju\n", (uintmax_t)sb->st_dev);
  printf("Inode:  %ju\n", (uintmax_t)sb->st_ino);
  puts("");
}

void print_file(const char *pathname) {
  struct stat sb;
  if (stat(pathname, &sb) == -1) {
    fprintf(stderr, "Error in stat %s: %s\n", pathname, strerror(errno));
    exit(EXIT_FAILURE);
  }
  print_sb(&sb);
}

void print_file_fd(int fd) {
  struct stat sb;
  if (fstat(fd, &sb) == -1) {
    perror("Error in stat");
    exit(EXIT_FAILURE);
  }
  print_sb(&sb);
}
