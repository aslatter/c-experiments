
#pragma once

#ifdef __clang_analyzer__
#define NORETURN __attribute__((analyzer_noreturn))
#else
#define NORETURN
#endif

void error_and_exit(char *msg) NORETURN;
__attribute__((format(printf, 1, 2))) void errno_and_exit(const char *format,
                                                          ...) NORETURN;
