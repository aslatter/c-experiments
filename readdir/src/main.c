
#include "main.h"

#include <dirent.h>
#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    error_and_exit("Missing argument: directory to open");
  }
  if (argc > 2) {
    error_and_exit("Too many arguments");
  }

  argv++;
  char *dir_name = *argv;

  // open directory
  DIR *dir = opendir(dir_name);
  if (!dir) {
    errno_and_exit("Error opening directory %s", dir_name);
  }

  // list contents of directory
  struct dirent *ent = NULL;
  while (true) {
    errno = 0;
    ent = readdir(dir);
    if (!ent) {
      if (errno) {
        errno_and_exit("Error reading from directory %s", dir_name);
      } else {
        break;
      }
    }

    puts(ent->d_name);
  }

  closedir(dir);
}

void error_and_exit(char *msg) {
  fprintf(stderr, "%s\n", msg);
  exit(EXIT_FAILURE);
}

void errno_and_exit(const char *format, ...) {
  int old_errno = errno;

  va_list ap;
  va_start(ap, format);
  vfprintf(stderr, format, ap);
  va_end(ap);

  fprintf(stderr, ": %s\n", strerror(old_errno));

  exit(EXIT_FAILURE);
}
