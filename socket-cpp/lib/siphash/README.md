
From: https://github.com/veorq/SipHash

Reference implementation of SipHash, a family of pseudorandom functions optimized for speed on short messages.

SipHash was designed as a mitigation to hash-flooding DoS attacks. It is now used in the hash tables implementation of Python, Ruby, Perl 5, etc.

SipHash was designed by Jean-Philippe Aumasson and Daniel J. Bernstein.
