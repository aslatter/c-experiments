#include "pipe.hh"

#include <unistd.h>

namespace io {

util::Result<Pipe> Pipe::create() {
  std::array<int, 2> pipefd{};

  if (pipe(pipefd.data()) == 0) {
    return Pipe(pipefd[0], pipefd[1]);
  }
  return util::make_posix_error(errno);
}

Pipe::Pipe(int read, int write) : read_{read}, write_{write} {}

} // namespace io