
#pragma once

#include "fd.hh"
#include "util/result.hh"

#include <optional>

namespace io {

// wrapper for the 'pipe' system function
class Pipe {
public:
  static util::Result<Pipe> create();
  const Fd &read_fd() { return this->read_; }
  const Fd &write_fd() { return this->write_; }

  ~Pipe() = default;

  // no copy, move okay
  Pipe(const Pipe &other) = delete;
  Pipe &operator=(const Pipe &other) = delete;

  Pipe(Pipe &&other) = default;
  Pipe &operator=(Pipe &&other) = default;

private:
  Fd read_;
  Fd write_;

  Pipe(int read, int write);
};

} // namespace io