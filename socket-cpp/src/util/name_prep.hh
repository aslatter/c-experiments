#pragma once

#include "result.hh"

#include <string>
#include <system_error>

namespace util {

// NFKC + case fold, over utf8 input
Result<std::string> name_prep(std::string_view name);

const std::error_category &string_format_err_category();

} // namespace util
