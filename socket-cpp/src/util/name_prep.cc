#include "name_prep.hh"

#include "fmt/format.h"

#include <unicode/bytestream.h>
#include <unicode/errorcode.h>
#include <unicode/normalizer2.h>

#include <cstdio>

namespace {
struct StringFormatErrCategory : std::error_category {
  const char *name() const noexcept override;
  std::string message(int ev) const override;
} format_error_category;

const char *StringFormatErrCategory::name() const noexcept {
  return "string_format";
}

std::string StringFormatErrCategory::message(int ev) const {
  auto ec = icu::ErrorCode();
  ec.set(static_cast<UErrorCode>(ev));
  auto name = std::string{ec.errorName()};
  return name;
}

std::error_code make_icu_code(const icu::ErrorCode &ec) {
  return std::error_code(static_cast<int>(ec.get()), format_error_category);
}

} // namespace

namespace util {

Result<std::string> name_prep(std::string_view name) {
  auto ec = icu::ErrorCode{};
  const auto *n = icu::Normalizer2::getNFKCCasefoldInstance(ec);
  if (ec.isFailure() != 0) {
    return make_error(make_icu_code(ec));
  }

  auto out = std::string{};
  auto sink = icu::StringByteSink<std::string>(&out);

  auto options = (uint32_t)0;
  n->normalizeUTF8(options, name, sink, nullptr, ec);
  if (ec.isFailure() != 0) {
    return make_error(make_icu_code(ec));
  }

  return out;
}

const std::error_category &string_format_err_category() {
  return format_error_category;
}

} // namespace util
