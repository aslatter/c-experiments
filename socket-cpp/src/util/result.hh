#pragma once

#include <system_error>
#include <variant>

// Similar to a variant for return success | okay, except
// on success has some of the niceties of std::optional like
// operator bool and operator*.

namespace util {

struct ResultError {
  std::error_code error;
};

// TODO - we should allow constructing directly from a std::error_code, maybe?
template <typename T> struct Result {
public:
  using index_error_t = std::in_place_index_t<1>;
  static constexpr index_error_t index_error = std::in_place_index<1>;

  typedef std::variant<T, std::error_code> data;

private:
  data data_;

public:
  template <typename... Args,
            typename = std::enable_if_t<std::is_constructible_v<
                data, std::in_place_index_t<0>, Args...>>>
  Result(Args &&... args) // NOLINT
      : data_(std::in_place_index<0>, std::forward<Args>(args)...) {}

  template <typename... Args,
            typename = std::enable_if_t<
                std::is_constructible_v<data, index_error_t, Args...>>>
  Result(index_error_t index, Args &&... args) // NOLINT
      : data_{index, std::forward<Args>(args)...} {}

  // from error-object
  Result(const ResultError &err) // NOLINT
      : data_{std::in_place_index<1>, err.error} {}

  bool is_okay() const { return this->data_.index() == 0; }
  bool is_error() const { return !this->is_okay(); }
  explicit operator bool() const { return this->is_okay(); }

  T &operator*() { return std::get<T>(this->data_); }
  const T &operator*() const { return std::get<T>(this->data_); }

  T *operator->() { return &std::get<T>(this->data_); }
  const T *operator->() const { return &std::get<T>(this->data_); }

  std::error_code &error() { return std::get<std::error_code>(this->data_); }
  const std::error_code &error() const {
    return std::get<std::error_code>(this->data_);
  }
};

inline ResultError make_error(const std::error_code &ec) {
  return ResultError{.error = ec};
}

inline std::error_code make_posix_error_code(int err) {
  return std::make_error_code(static_cast<std::errc>(err));
}

inline ResultError make_posix_error(int err) {
  return make_error(make_posix_error_code(err));
}

} // namespace util