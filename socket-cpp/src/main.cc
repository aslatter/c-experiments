
#include "http.hh"
#include "io/reader.hh"
#include "net/http/request.hh"
#include "net/sock.hh"
#include "options.hh"

#include "fmt/format.h"

#include <iostream>
#include <regex>
#include <sstream>

#include <cctype>
#include <cstdio>
#include <unistd.h>

#define UNUSED __attribute__((unused)) // NOLINT(cppcoreguidelines-macro-usage)

int main(UNUSED int argc, const char **argv) {
  auto opts = Options::parse(argv);

  // take user-input host-name and convert to form
  // suitable for DNS lookup &c. Many characters are
  // normalized, and code-points >127 are punycoded.
  auto host = http::format_host(opts.node);

  net::sock_hints hints = {.family = 0, .type = net::sock_type_stream};
  auto s = net::connect(host, "http", hints);
  if (!s) {
    fmt::print("Unable to connect to '{}': {}\n", opts.node,
               s.error().message());
    return EXIT_FAILURE;
  }

  // send request
  auto err =
      net::http::send_get(s->file(), {.host = host, .resource = opts.resource});
  if (err) {
    fmt::print("Unable to send request: {}\n", err->message());
    return EXIT_FAILURE;
  }

  // get response
  std::string line{};
  auto r = io::Reader(*s);

  // output status and headers
  std::string response_headers{};
  while (r.read_line(&line)) {
    response_headers.append(line);
    response_headers.append("\n");
    if (line.empty()) {
      // done with headers
      std::cerr << response_headers;
      break;
    }
  }

  // output body
  r.write_to(STDOUT_FILENO);

  return 0;
}
