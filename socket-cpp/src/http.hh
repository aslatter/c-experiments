
#pragma once

#include <string>

namespace http {

// format a server-name for use in a 'host' header.
std::string format_host(const std::string &host_raw);

} // namespace http
