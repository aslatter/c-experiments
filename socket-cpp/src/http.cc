
#include "http.hh"

#include "net/punycode.hh"

#include <cassert>

namespace http {

std::string format_host(const std::string &host_raw) {
  auto host_coded = net::to_puny_code(host_raw);
  // remove newlines & whitespace to avoid header-injection
  std::string ret_val{};
  ret_val.reserve(host_coded.size());

  for (auto c : host_coded) {
    if (std::isspace(static_cast<unsigned char>(c)) == 0) {
      ret_val.push_back(c);
    }
  }
  return ret_val;
}

} // namespace http
