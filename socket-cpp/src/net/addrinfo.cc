#include "addrinfo.hh"

#include <netdb.h>
#include <optional>
#include <sys/socket.h>
#include <sys/types.h>

namespace net {

util::Result<AddrInfo> AddrInfo::lookup(const std::string &node,
                                        const std::string &service,
                                        const struct addrinfo *hints) {
  struct addrinfo *result = nullptr;
  if (getaddrinfo(node.c_str(), service.c_str(), hints, &result) != 0) {
    return util::make_posix_error(errno);
  }
  return AddrInfo(result);
}

void AddrInfo::deleter::operator()(struct addrinfo *ai) {
  if (ai != nullptr) {
    freeaddrinfo(ai);
  }
}

} // namespace net