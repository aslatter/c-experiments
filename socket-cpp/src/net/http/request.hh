#pragma once

#include "headers.hh"

#include <optional>
#include <string>
#include <system_error>

namespace net::http {

struct Request {
  std::string host;
  std::string resource;
  Headers headers = Headers();
};

std::optional<std::error_code> send_get(int s, const Request &req);

std::optional<std::string> request_to_string(const Request &req);

} // namespace net::http