
#include "request.hh"
#include "util/result.hh"

#include "fmt/format.h"

#include <unistd.h>

#include <cassert>
#include <cstdio>

namespace {

bool is_url_safe(char c) {
  if (c >= 'a' && c <= 'z') {
    return true;
  }
  if (c >= 'A' && c <= 'Z') {
    return true;
  }
  if (c >= '0' && c <= '9') {
    return true;
  }
  switch (c) {
  // generally safe
  case '-':
  case '_':
  case '.':
  case '~':
  // safe when used for their intended url-purposes
  case '/':
    return true;

  default:
    return false;
  }
}

void char_to_hex(char c, std::string *out) {
  assert(out != nullptr);

  // this feels like a silly way to do things
  // but it's easy.
  auto uc = static_cast<unsigned char>(c);

  const auto MAX_DECIMAL = 10;
  const auto CHAR_LOW_MASK = 0x0F;

  unsigned char high = uc >> 4;
  if (high < MAX_DECIMAL) {
    out->append(std::to_string(high));
  } else {
    out->push_back('A' + high - MAX_DECIMAL);
  }

  unsigned char low = uc & CHAR_LOW_MASK;
  if (low < MAX_DECIMAL) {
    out->append(std::to_string(low));
  } else {
    out->push_back('A' + low - MAX_DECIMAL);
  }
}

std::string format_resource(const std::string &resource_raw) {
  std::string ret_val{};
  ret_val.reserve(3 * resource_raw.size());

  // prefix with slash
  if (resource_raw.empty() || resource_raw[0] != '/') {
    ret_val.push_back('/');
  }

  // url escape
  for (auto c : resource_raw) {
    if (is_url_safe(c)) {
      ret_val.push_back(c);
      continue;
    }
    ret_val.push_back('%');
    char_to_hex(c, &ret_val);
  }

  return ret_val;
}

void append_header(fmt::memory_buffer *b, std::string_view key,
                   std::string_view value) {
  auto sanitized_key = std::string{};
  for (auto c : key) {
    // RFC 822 3.1.2
    if (c >= 33 && c <= 126 && c != ':') {
      sanitized_key.push_back(c);
    }
  }

  auto sanitized_value = std::string{};
  for (auto c : value) {
    // RFC 822 3.1.2
    // TODO - do I care about RFC 2616 4.2 multi-line headers?
    if (c != '\r' && c != '\n') {
      sanitized_value.push_back(c);
    }
  }
  fmt::format_to(*b, "{}: {}\r\n", sanitized_key, sanitized_value);
}

} // namespace

std::optional<std::error_code> net::http::send_get(int s, const Request &req) {
  auto raw_request = request_to_string(req);
  if (!raw_request) {
    // TODO - request_to_string should return a 'Result'
    return util::make_posix_error_code(EINVAL);
  }

  auto to_write = std::string_view(*raw_request);
  while (to_write.length() > 0) {
    // do write
    auto write_result = write(s, to_write.data(), to_write.length());
    if (write_result < 0) {
      if (errno != EINTR) {
        return util::make_posix_error_code(errno);
      }
      continue;
    }
    to_write.remove_prefix(write_result);
  }

  return {};
}

std::optional<std::string> net::http::request_to_string(const Request &req) {

  // copy incoming headers so we can default some in
  Headers h = req.headers;

  // we need a host-header from somewhere
  if (req.host.empty() && !h.has("Host")) {
    // TODO - return errors how?
    return {};
  }

  if (!h.has("User-Agent")) {
    h["User-Agent"] = "X-Demo-Agent";
  }
  if (!h.has("Host")) {
    h["Host"] = req.host;
  }
  if (!h.has("Connection")) {
    h["Connection"] = "close";
  }

  fmt::memory_buffer message{};

  auto resource = format_resource(req.resource);
  fmt::format_to(message, "GET {} HTTP/1.1\r\n", resource.c_str());

  for (const auto &[key, value] : h) {
    append_header(&message, key, value);
  }

  fmt::format_to(message, "\r\n");

  std::string result(message.data(), message.size());
  return result;
}