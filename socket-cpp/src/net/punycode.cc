#include "punycode.hh"

#include "util/name_prep.hh"

extern "C" {
#include "punycode.h"
}

#include <array>
#include <vector>

namespace {

bool char_requires_coding(char c) {
  return static_cast<unsigned char>(c) > 0x7F;
}

bool requires_puny_coding(std::string_view raw_str) {
  // any char > 0x7F
  for (auto c : raw_str) {
    if (char_requires_coding(c)) {
      return true;
    }
  }
  return false;
}

// utf-8 to code-point
// TODO - figure out the thing built-in to C++ stdlib, or
//   use macros in ICU?
class code_point_iter {
  // we need 21 + 1 bits to hold the max code-point we can encode with utf8
  static_assert(sizeof(int) >= 3);
  std::string_view str;
  int current_char;

public:
  explicit code_point_iter(std::string_view str) : str{str}, current_char{0} {
    (*this)++;
  }

  bool done() const { return this->current_char == -1; }
  int operator*() const { return this->current_char; }
  int operator++(int) {
    auto old = **this;

    auto remainder_len = this->str.length();
    if (remainder_len == 0) {
      this->make_zero();
      return old;
    }

    auto byte0 = static_cast<unsigned char>(this->str[0]);
    this->str.remove_prefix(1);

    int accum = 0;
    size_t remaining_bytes = 0;

    if ((byte0 & 0b10000000) == 0) {
      accum = byte0 & 0b01111111;
      remaining_bytes = 0;
    } else if ((byte0 & 0b11100000) == 0b11000000) {
      accum = static_cast<int>(byte0 & 0b00011111) << 6;
      remaining_bytes = 1;
    } else if ((byte0 & 0b11110000) == 0b11100000) {
      accum = static_cast<int>(byte0 & 0b00001111) << 12;
      remaining_bytes = 2;
    } else if ((byte0 & 0b11111000) == 0b11110000) {
      accum = static_cast<int>(byte0 & 0b00000111) << 18;
      remaining_bytes = 3;
    }
    if (remaining_bytes > this->str.length()) {
      // invalid
      this->make_zero();
      return old;
    }
    while ((remaining_bytes--) != 0) {
      auto byte = static_cast<unsigned char>(this->str[0]);
      this->str.remove_prefix(1);
      // validate
      if ((byte & 0b11000000) != 0b10000000) {
        this->make_zero();
        return old;
      }
      // accumulate
      accum += static_cast<int>(byte & 0b00111111) << (remaining_bytes * 6);
    }
    this->current_char = accum;
    return old;
  }

private:
  void make_zero() {
    this->str = "";
    this->current_char = -1;
  }
};

void punnycode_segment(std::string *dest, std::string_view input_segment) {
  dest->append("xn--");

  // first convert to code-points
  auto code_points = std::vector<punycode_uint>{};
  for (auto iter = code_point_iter(input_segment); !iter.done(); iter++) {
    code_points.push_back(*iter);
  }

  auto length = static_cast<punycode_uint>(input_segment.length() * 8);
  auto orig_length = length;
  auto output = std::string(length, 0);
  while (true) {

    auto result = punycode_encode(code_points.size(), code_points.data(),
                                  nullptr, &length, output.data());
    switch (result) {
    case punycode_status::punycode_success:
      output.resize(length);
      dest->append(output);
      return;

    case punycode_status::punycode_big_output:
      // double it?
      orig_length *= 2;
      length = orig_length;
      output.resize(length, 0);
      continue;

    default:
      // uh--oh
      // TODO - return failure somehow
      return;
    }
  }
}

} // namespace

namespace net {

std::string to_puny_code(std::string_view raw_str) {
  auto normalized_result = util::name_prep(raw_str);
  if (!normalized_result) {
    // what?
    // TODO - do something
    return std::string(raw_str);
  }
  auto normalized = std::move(*normalized_result);

  if (!requires_puny_coding(normalized)) {
    return normalized;
  }

  auto result = std::string();

  // puny-code each dot-separated segment
  auto input_segment = std::string{};
  auto segment_requires_coding = false;
  for (auto c : normalized) {
    if (char_requires_coding(c)) {
      segment_requires_coding = true;
    }
    if (c != '.') {
      input_segment.push_back(c);
      continue;
    }
    // c == '.' -> segment boundary
    if (!segment_requires_coding) {
      result.append(input_segment);
    } else {
      punnycode_segment(&result, input_segment);
    }
    result.push_back('.');
    segment_requires_coding = false;
    input_segment.clear();
  }
  if (input_segment.length() != 0) {
    if (!segment_requires_coding) {
      result.append(input_segment);
    } else {
      punnycode_segment(&result, input_segment);
    }
  }

  return result;
}

} // namespace net
