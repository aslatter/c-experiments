
#include "sock.hh"

#include "addrinfo.hh"

#include <iostream>
#include <optional>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

namespace {

util::Result<io::Fd> sock_create(int domain, int type, int protocol) {
  auto fd = socket(domain, type, protocol);
  if (fd > -1) {
    return io::Fd{fd};
  }
  return util::make_posix_error(errno);
}

} // namespace

namespace net {

// I'm not super happy with how 'hints' are passed
// down. Maybe we should just have separate 'connect
// stream' vs 'connect dgram'?

util::Result<io::Fd> connect(const std::string &node,
                             const std::string &service,
                             const sock_hints &hints) {

  struct addrinfo ai_hints = {};
  ai_hints.ai_family = hints.family;
  ai_hints.ai_socktype = hints.type;

  auto info = AddrInfo::lookup(node, service, &ai_hints);
  if (!info) {
    return util::make_error(info.error());
  }

  auto last_error = std::error_code{};
  for (const auto *r : *info) {
    auto s = sock_create(r->ai_family, r->ai_socktype, r->ai_protocol);
    if (!s) {
      last_error = s.error();
      continue;
    }
    // TODO - handle EINTR
    if (::connect(s->file(), r->ai_addr, r->ai_addrlen) != 0) {
      last_error = util::make_posix_error_code(errno);
      continue;
    }
    // connected!
    return s;
  }

  // could not connect
  return util::make_error(last_error);
}

util::Result<io::Fd> connect(const std::string &node, uint16_t service,
                             const sock_hints &hints) {
  return connect(node, std::to_string(service), hints);
}

util::Result<io::Fd> connect(const std::string &node,
                             const std::string &service) {
  return connect(node, service, {});
}

util::Result<io::Fd> connect(const std::string &node, uint16_t service) {
  return connect(node, service, {});
}

const int sock_family_inet = AF_INET;
const int sock_family_inet6 = AF_INET6;

const int sock_type_dgram = SOCK_DGRAM;
const int sock_type_stream = SOCK_STREAM;

} // namespace net