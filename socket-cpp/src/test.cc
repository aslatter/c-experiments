
#include "net/http/headers.hh"
#include "net/punycode.hh"
#include "util/name_prep.hh"

#include "fmt/format.h"

#include <functional>
#include <string>
#include <vector>

#include <cstdio>

int main() {
  net::http::Headers h{};

  h["FOO"] = "foo?";
  h["Foo"] = "Bar";
  h["foo"] = "quux";

  h["bar"] = "foo";

  for (const auto &[key, value] : h) {
    fmt::print("{}: {}\n", key, value);
  }

  // direct hash tests
  fmt::print("\n");

  const auto *valid_name = u8"Cliché";
  auto valid_name_result = util::name_prep(valid_name);

  if (valid_name_result) {
    fmt::print("valid_name: {}\n", *valid_name_result);
  } else {
    fmt::print("expected valid case was not a valid name: {}\n",
               valid_name_result.error().message());
  }

  // I was hoping this would be a test of failure, but
  // I can't make util::name_prep fail?!
  const auto *invalid_name = "\xF0";
  auto invalid_name_result = util::name_prep(invalid_name);

  if (invalid_name_result) {
    fmt::print("invalid_name: {}\n", *invalid_name_result);
  } else {
    fmt::print("expected invalid case was not a valid name: {}\n",
               invalid_name_result.error().message());
  }

  fmt::print(u8"www.ＫＡＤＯＫＡＷＡ-Ω.Com: {}\n",
             net::to_puny_code(u8"www.ＫＡＤＯＫＡＷＡ-Ω.Com"));
}
