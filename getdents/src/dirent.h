
#pragma once

#define _GNU_SOURCE
#include <stddef.h>
#include <sys/stat.h>
#include <sys/types.h>

struct linux_dirent64 {
  ino64_t d_ino;           /* 64-bit inode number */
  off64_t d_off;           /* 64-bit offset to next structure */
  unsigned short d_reclen; /* Size of this dirent */
  unsigned char d_type;    /* File type */
  char d_name[];           /* Filename (null-terminated) */
};

extern long getdents64(int fd, void *ents, size_t len);
