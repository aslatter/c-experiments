
#include "dirent.h"

#include <sys/syscall.h>
#include <unistd.h>

long getdents64(int fd, void *ents, size_t count) {
  return syscall(SYS_getdents64, fd, ents, count);
}
