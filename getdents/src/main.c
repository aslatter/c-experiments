
#include "dirent.h"

#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFF_LEN 5000

int main(int argc, char **argv) {
  if (argc < 2) {
    fprintf(stderr, "Missing directory argument\n");
    exit(EXIT_FAILURE);
  }

  if (argc > 2) {
    fprintf(stderr, "Too many arguments\n");
    exit(EXIT_FAILURE);
  }

  argv++;
  char *dirname = *argv;

  // open directory
  int fd = open(dirname, O_DIRECTORY | O_RDONLY);
  if (fd < 0) {
    perror("open");
    exit(EXIT_FAILURE);
  }

  // loop over entries
  // a dirent is not a fixed-size thing, so
  // we can only receive them in a char* buffer.
  // However each dirent has a fixed-position field
  // with its length, so we can use that to advance
  // through the buffer.
  char *base_buff = malloc(BUFF_LEN);
  char *buff = base_buff;
  while (true) {
    long count = getdents64(fd, buff, BUFF_LEN);

    if (count < 0) {
      perror("getdents64");
      free(base_buff);
      close(fd);
      exit(EXIT_FAILURE);
    }

    if (count == 0) {
      // done!
      break;
    }

    while (count > 0) {
      struct linux_dirent64 *entry = (struct linux_dirent64 *)buff;

      puts(entry->d_name);

      unsigned short reclen = entry->d_reclen;
      buff += reclen;
      count -= reclen;
    }
  }

  free(base_buff);
  close(fd);
}
